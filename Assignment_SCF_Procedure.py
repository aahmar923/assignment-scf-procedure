from scipy.integrate import quad
import numpy as np
import math as mt
from scipy import linalg
# Define the Gaussian function
def gaussian_function(alpha, R, r,x):
    return x*(np.exp(-alpha * np.linalg.norm(r - R)**2))

# Function to calculate overlap integral between two Gaussian functions
def overlap_integral(alpha_i, alpha_j, Ri, Rj):
    integrand = lambda r: gaussian_function(alpha_i, Ri, r,x=2.144803129) * gaussian_function(alpha_j, Rj, r,x=0.5221674274)
    integral, _ = quad(integrand, -np.inf, np.inf)
    return integral

# Example parameters
alpha_i = 4.3448
alpha_j = 0.66049
Ri = np.array([0, 0, 0])
Rj = np.array([1.40106, 0, 0])

# Calculate overlap integral
S_ij = overlap_integral(alpha_i, alpha_j, Ri, Rj)
S_mtrix=np.array([[1,S_ij],[S_ij,1]])
print("Overlap integral S_ij:", S_mtrix)
def K_E(alpha_i,alpha_j):
    T=abs((alpha_i*alpha_j)/(alpha_i+alpha_j)*(3-2*(alpha_i*alpha_j)*(Ri[0]-Rj[0])**2)*(mt.pi/(alpha_i+alpha_j)**(3/2))*(mt.exp(-(alpha_i*alpha_j)/(alpha_i+alpha_j)*(Ri[0]-Rj[0])**2)))
    return T
K_E_matrix=np.zeros((2,2))
K_E_matrix[0][0]=K_E(alpha_i,alpha_i)
K_E_matrix[0][1]=K_E(alpha_i,alpha_j)
K_E_matrix[1][0]=K_E(alpha_i,alpha_j)
K_E_matrix[1][1]=K_E(alpha_j,alpha_j)

print("kinetic enrgy =  ",K_E_matrix)


def attrection_integral(alpha_i, alpha_j, Ri, Rj):
    integrnd = lambda r: gaussian_function(alpha_i, Ri, r,x=2.144803129)*(-1/(r-Ri[0])) * gaussian_function(alpha_j, Rj, r,x=0.5221674274)
    integral, _ = quad(integrnd, -np.inf, np.inf)
    return integral
atrection=np.zeros((2,2))
atrection[0][0]= attrection_integral(alpha_i, alpha_i, Ri, Rj)
atrection[0][1]= attrection_integral(alpha_i, alpha_j, Ri, Rj)
atrection[1][0]= attrection_integral(alpha_i, alpha_j, Ri, Rj)
atrection[1][1]= attrection_integral(alpha_j, alpha_j, Ri, Rj)
print(atrection)

H_core=np.zeros((2,2))
for i in range(2):
    for j in range(2):
        H_core[i][j]=atrection[i][j]+K_E_matrix[i][j]

print("H_core\n",H_core)
#let densty matrix
P=np.zeros((2,2))
#because density matrix zero G will be zero
G=np.zeros((2,2))

F=np.zeros((2,2))
#because G is zero sum of H_core and G will be H_core
F=H_core
print("F is\n",F)
X=np.zeros((2,2))
for x in range(2):
    for y in range(2):
        X[x][y]=(S_mtrix[x][y])**(-1/2)
print("X is\n",X)
X_harmitian=np.zeros((2,2))
X_harmitian=X.transpose()

print("X harmitian is\n",X_harmitian)

tmp=np.zeros((2,2))
F_P=np.zeros((2,2))
for x in range(2):
    for y in range(2):
        for z in range(2):
            tmp[x][y]+=(F[x][z])*(X_harmitian[z][y])

for x in range(2):
    for y in range(2):
        for z in range(2):
            F_P[x][y]+=(tmp[x][z])*(X[z][y])

print("F prime  is\n",F_P)

eigenvalues, eigenvectors = linalg.eig(F_P)
print("F eign values  is\n",eigenvalues)
C_P= np.dot(X, eigenvectors)
C= np.dot(X, C_P)
print("C prime  is\n",C_P)
print("C  is\n",C)


occupation = 2.0
for i in range(2):
    for j in range(2):
        # mo is (natomic_orbtials x nMOs)
        for oo in range(1):
            
            Cpm = C[i, oo]
            C_dagger = C[j, oo]
            P[i,j] += occupation * Cpm * C_dagger 
print("density is\n",P)
